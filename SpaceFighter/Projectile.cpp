
#include "Projectile.h"

Texture *Projectile::s_pTexture = nullptr;

Projectile::Projectile()
{
	SetSpeed(500);
	SetDamage(1);
	SetDirection(-Vector2::UNIT_Y);
	SetCollisionRadius(9);

	m_drawnByLevel = true;
}

void Projectile::Update(const GameTime *pGameTime)
{
	if (IsActive()) //Checks to see if a projectile is active
	{
		Vector2 translation = m_direction * m_speed * pGameTime->GetTimeElapsed(); 
		//Defines the vector used to move the projectile
		TranslatePosition(translation);
		//Calls the translate position function using the above vector as the parameter.

		Vector2 position = GetPosition(); //gets the postion of the projectile
		Vector2 size = s_pTexture->GetSize(); //gets the size of the projectile

		// Is the projectile off the screen?
		if (position.Y < -size.Y) Deactivate(); 
		//checks to see if the y position of the projectile is less than its height.
		else if (position.X < -size.X) Deactivate(); 
		//checks to see if the x position of the projectile is less than its width
		else if (position.Y > Game::GetScreenHeight() + size.Y) Deactivate();
		//checks to see if the object is somewhere off screen vertically by using the game screen's height.
		else if (position.X > Game::GetScreenWidth() + size.X) Deactivate();
		//checks to see if the object is somewhere off screen horizontally by using the game screen's width.
	}

	GameObject::Update(pGameTime); //Standard object update.
}

void Projectile::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(s_pTexture, GetPosition(), Color::White, s_pTexture->GetCenter());
	}
}

void Projectile::Activate(const Vector2 &position, bool wasShotByPlayer)
{
	m_wasShotByPlayer = wasShotByPlayer;
	SetPosition(position);

	GameObject::Activate();
}

std::string Projectile::ToString() const
{
	return ((WasShotByPlayer()) ? "Player " : "Enemy ") + GetProjectileTypeString();
}

CollisionType Projectile::GetCollisionType() const
{
	CollisionType shipType = WasShotByPlayer() ? CollisionType::PLAYER : CollisionType::ENEMY;
	return (shipType | GetProjectileType());
}